# Ny bruker

Som medlem av RadiOrakel vil vi gjerne at du legger ut innhold på
nettsidene våre. Ta kontakt med IT-sjef på <itsjef@radiorakel.no> eller
slack for å opprette en bruker, så får du en epost med link til å lage
passord. Brukeren din blir synlig på radiorakel.no/author/navn, se
[radiorakel.no/author/sara.ambjoerndalen](http://radiorakel.no/author/sara.ambjoerndalen)
for et eksempel.

Du logger inn på [radiorakel.no/login](https://radiorakel.no/login).

## Brukernivåer

- Skribent - Alle kan få en slik konto. Kan skrive innlegg som
  godkjennes av redaksjonsrådet før publisering.
- Redaksjonsmedlem - For medlemmer av redaksjoner. Kan skrive innlegg
  og publisere uten godkjenning.
- Program - Kan ikke logge inn. Får en
  [programside](programside.md)
- Editor - For medlemmer av redaksjonsrådet. Kan publisere innlegg og
  sider, redigere andres innlegg og brukerprofiler.
- Administrator - Alle rettigheter.

## Profilinnstillinger

Ved første innlogging anbefales det å gå til **Profile** i hovedmenyen
for å oppdatere brukeren din. Her kommer en kort oversikt over punktene,
de viktigste først.

Rediger bruker.

![](bruker_innstillinger.png)

### Name

Her kan du endre hvordan navnet ditt vises på hele nettsiden. Som
standard putter vi inn navnet ditt fra medlemslistene.

- Username - kan ikke forandres.
- First name / Last name - Selvforklarende.
- Nickname - Hvis du vil bruke et kortere navn / kallenavn.
- Display name publicly as - Her velger du hvordan navnet ditt vises
  på hele nettsiden.

### About Yourself

Her fyller du inn litt informasjon om deg selv. Dette er infoen som blir
publisert på profilsiden din. Ikke velg profilbildet ditt her, det
gjøres mye enklere under **Avatar** nederst på siden.

### Avatar

Last opp et probilbilde. Kvadratisk, minst 1400x1400px, under
3000x3000px. Ikke over 1MB.

### Contact Info (valgfritt)

- Email - Den kan byttes, men ta heller kontakt med daglig leder for å
  bytte eposten din sentralt for all kontakt med radioen.
- Website - Hvis du har en nettside du vil knytte til profilen din.

Feltene som AIM, Yahoo osv er helt valgfri hvis du vil la lesere komme i
kontakt med deg. Du kan sette mer "moderne" sosiale medier i neste
punkt.

### Social Profiles (valgfritt)

Her kan du fylle inn en haug av forskjellige sosiale medier. Det dukker
opp linker til disse på profilsiden din.
