# Podkast

Denne guiden gjelder kun for å lage RSS-feeds som lyttere kan abonnere
på.

## Ny podkast

Eksempel på podkastside.

![](podkast_landing_page.png)

Før første episode kan legges ut må det gjøres litt oppsett. Resultatet
er at podkasten din blir tilgjengelig i iTunes, spotify og de fleste
steder hvor man kan abonnere på podkaster. Du får også en fin side med
mediespiller og linker til å abonnere. Ta kontakt med IT-sjef på slack
for å komme igang, og ha informasjonen under klar.

Nødvendig informasjon:

- Programnavn
- Veldig kort beskrivelse av programmet.
- Lengre beskrivelse av programmet - maks 4000 tegn.
- Hvilke
  [itunes-kategorier](https://castos.com/itunes-podcast-category-list/)
  det passer i. Maks 3.
- Hovedspråk
- Om du har _explicit content_ (Ja/Nei).
- Om du vil vise siste episoder først eller sist.
- Kvadratisk bilde. Minst 1400x1400 piksler i jpg eller png. ikke
  større enn 3000x3000.

IT-sjef oppretter podkasten på nettsiden. Når det er klart er det på
tide å legge inn første epsiode. Følg guiden under for å legge ut et par
episoder. Når de ligger ute kan vi lage podkastsiden og sende inn
podkasten til godkjenning i iTunes og andre steder.

## Ny episode

### Klargjør lydfil

Lydfilen skal være i mp3-format med 128Kbps, med Constant Bit Rate
(CBR). Merk den på en fornuftig måte som er konsistent med tittel på
innlegget du lager.

### Last opp lydfil

Logg deg inn på [radiorakel.no](https://radiorakel.no/login), gå til
**Media -\> Add New** og last opp lydfilen. Gå så til **Media -\>
Library**, finn filen du lastet opp og kopier **File URL**. Dette er
linken til lydfilen, som du må legge inn i selve innlegget.

### Skriv innlegg

Trykk på **Posts -\> add new**.

- Lim inn **file URL** nederst under Podcast Episode -\> Media URL og
  trykk Verify URL.
- Tittel - dette blir tittelen på podkasten både i iTunes og nettsia.
- Legg til minst ett bilde i innlegget. Det første bildet i innlegget
  vises på forsiden av radiorakel.no og i podkastleseren.
- Kategorier - den eneste kategorien som skal være valgt er **podkast
  -\> programnavn**, det er dette som gjør at innlegget blir
  tilgjengelig som podkast.
- Skriv litt om hva episoden handler om, dette dukker opp i
  podkastleseren.
- Trykk Publish!
- Sjekk at innlegget ser bra ut og at lydfilen kan spilles av.
- Sjekk at lydfilen dukker opp på podkastsiden din, typisk
  radiorakel.no/programnavn

Les også hvordan du kan [publisere innlegg](publiser.md)
