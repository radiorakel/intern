# Publiser innlegg

Gå til **Posts -\> Add New** i menyen til venstre.

Du får da opp dette vinduet.

![](publiser_addnew.png)

Publiseringsverktøyet oppfører seg ganske likt som word og andre
tekstredigeringsprogrammer. Trykk _Add Media_ for å legge inn bilde
eller lyd.

Under går vi gjennom feltene som alltid må fylles ut.

## Forfattere

Forfatterfeltet

![](publiser_author.png)

I forfatterfeltet legger du til medforfatterne til innlegget du skriver.
Det gjør at innlegget dukker opp på forfattersiden til hvert innlegg.
Hvis innlegget er knyttet til et program puttes programmet på første
linje.

## Kategorier

Under _Categories_ velger du hovedtemaet for artikkelen din. (Velg en).
Det gjør at vi kan gruppere innhold i ettertid.

## Tags

![](publiser_tags.png)

Tags er nøkkelord som gjør det lettere å søke seg fram til artikkelen
din og som gjør at vi liste opp tematisk like innlegg. Her kan du gjerne
velge flere og legge til nye.

## Featured image

![](publiser_featured.png)

Under _Featured Image_ velger du et bilde som hører til innlegget. Dette
bildet vises på alle steder på nettsiden som har oversikt over innlegg,
f.eks på forsiden.

Bildet bør ikke være over 1MB. Da er det bedre å nedskalere det før
opplasting.

## Publiser

Før du publiserer innlegget er det lurt å trykke **Preview** for å
sjekke at innholdet ser fint ut. Gjør eventuelle forbedringer og trykk
**Publiser**!
