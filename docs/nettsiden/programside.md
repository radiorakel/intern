# Programside

En programside består av et profilbilde, informasjon om programmet og
alle innlegg programmet har skrevet. Programsider på nettsiden havner i
[oversikten over programmer](https://radiorakel.no/programmer).

Noen eksempler på programsider

- [Flamme snakk](https://radiorakel.no/author/flamme-snakk/)
- [Ugress](https://radiorakel.no/author/ugress/)

## Opprett

For å starte trengs

- Programtekst
- Kvadratisk bilde. Minst 1400x1400 piksler i jpg eller png. ikke
  større enn 3000x3000.
