# Velkommen

RadiOrakel er kanskje verdens eldste feministradio og vi er stolte over at du
vil være med på laget! Vårt mål er å lage radio av høy standard både
innholdsmessig og teknisk, og vi oppmuntrer medlemmene til innsats for
alternativ, eksperimentell og kritisk journalistikk.

RadiOrakels journalistiske virksomhet skal være i tråd med RadiOrakels
[vedtekter](vedtekter.md) spesielt, og ellers følge retningslinjene i Vær
Varsom-plakaten.

Våre forskjellige kurs innen journalistikk, lydteknikk og radioproduksjon
generelt kan kanskje bli din vei videre!

Som medlem hos oss får du tilgang til arbeidsplass og utstyr til å lage radio og
publisere saker på nett. Du kan blant annet være programleder / tekniker,
produsere innslag / podcast, få pressekort / akkreditering til konserter og
begivenheter, mm.

Ta kontakt med [redaktør](mailto:redaktor@radiorakel.no) ,
[teknisk sjef](mailto:teknisksjef@radiorakel.no) eller
[daglig leder](mailto:dagligleder@radiorakel.no) for mer informasjon om
mulighetene på RadiOrakel.

Vi bruker Slack som samarbeidsplattform, og det er her du får raskest hjelp med
alt du skulle lure på – se på det som en slags privat facebook for våre
medlemmer. Du har fått en epost med invitasjon, si fra til IT-sjef hvis den ikke
har kommet fram. Har du smarttelefon anbefaler vi å laste ned slack-appen.
Innlogging fra PC via [slack.radiorakel.no](https://slack.radiorakel.no).

Det trådløse nettet på radioen heter rOr. Koden får du ved å skrive
'kode internett' i slack.

Reservasjon av studio skjer fra
[reservasjon.radiorakel.no](https://reservasjon.radiorakel.no).
Brukernavnet er din personlige epost og du kommer i gang ved å trykke på 'I forgot my
password'.

Vi håper du har mange ideer og at vi kan være springbrettet som sender dem ut i
verden!

Hjertelig hilsen

Heid Jerstad - Ansvarlig redaktør
