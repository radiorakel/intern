# Ordliste

## Innhold

- **Spor/kutt** - Sanger/avsnitt på plate, CD eller minidisk.
- **Stikk** - Snakk i studio. Enten mellom låter/ innslag på direkten,
  eller i et innslag.
- **Innslag** - Alt du sender på lufta som ikke er deg som prater
  eller ren musikk. Et innslag er som regel tatt opp på forhånd, med
  mindre du har gjest i studio på direkten eller per telefon.
- **Intro** - Kort stikk før et innslag/gjest som forteller hva som
  kommer rett etter.
- **Utro** - Kort stikk etter et innslag/ gjest som kort oppsummerer
  hva lytteren nettopp hørte, og gjentar dato for f.eks en konsert
  eller lignende.
- **Innslag** - Alt du sender på lufta som ikke er deg som prater
  eller ren musikk. Et innslag er som regel tatt opp på forhånd, med
  mindre du har gjest i studio på direkten eller per telefon.
- **Kjenning** - Når et radioprogram begynner har det gjerne en
  åpningsmelodi, eller lyder som markerer overgangen fra forrige
  program. Hvis det er flere radiostasjoner som deler frekvens, har
  radioen også en egen kjenning.
- **Jingle** - En kort lydsnutt med snakk på, som forteller deg
  hvilken kanal/program du hører på. Skal spilles ca en gang i
  kvarteret, for at lytteren til enhver tid skal vite hva hun hører
  på.
- **Teaser** - Kan være en kort lydsnutt fra et innslag du skal sende
  i løpet av programmet, eller at du forteller lytteren hva som skal
  skje i programmet ditt. For å skape interesse, og holde lytteren på
  frekvensen. Kommer tidligere enn introen.
- **Spot** - Reklame. Vår frekvens er reklamefri, men vi lager spotter
  for nye sendinger, journalistkurs, radioserier, konserter vi sender
  live fra o.l. Kan vare opp mot to minutter, med stikk og kontentum.
- **Kontentum** - Bakgrunnslyd/effektlyd. F. eks. lyden av en hund som
  bjeffer når du snakker om hunder, eller musikk som går i bakgrunnen
  under et stikk.

## Teknikk

- **Monitor** - Høyttaler
- **Kompressor** - En kompressor begrenser lydnivået ved opptak eller
  når du er på lufta for at lyden ikke skal overstyre og vrenge seg.
- **Lydkilde** - Rett og slett noe det kommer lyd fra. Kilde til lyd.
- **Feedback** - Rundgang av lyd fra mikrofon via høyttaler tilbake
  til mikrofon. Høy, ekkel vedvarende piping.
- **Talkback** - Kommunikasjonen mellom studio og tekniker. Går ikke
  på lufta.
- **Å fade** - Å øke eller senke lyden gradvis (å fade opp eller fade
  ned). Gjør at låter ikke avsluttes så brått.
- **Å krysse** - Senke en lydkilde mens man øker en annen. Når man
  spiller to låter etter hverandre, da kan man krysse med en jingel
  mellom (låt -- jingel -- låt)
- **Å cue** - Gjøre klar en lydkilde for avspilling, så det for
  eksempel ikke er stille tre sekunder i starten av en låt

## Annet

- **På lufta** - Det røde lyset er på, og alt det du sier i studio
  kommer ut på lufta eller inn på opptaket
- **Råmateriale** - Opptak som du ikke har begynt å redigere enda, det
  vil si; originalopptaket.
- **Redigere** - Sette sammen råmateriale og blande forskjellige
  lydkilder til et ferdig innslag.
- **Blokker** - En blokk er en lydsnutt som du har inne i
  dataredigeringsprogrammet. Å blokke noe betyr å dele det opp i biter
  for så å legge det i ønsket rekkefølge med litt rom mellom. Det er
  ofte det første man gjør med råmaterialet før videre redigering.
- **Research** - Forberedelse til sending/innslag. Å finne ut fakta på
  forhånd før du skal gjøre et intervju, ha en sending eller lage et
  innslag.
