# PCer

## Lagring av filer

### Opptakere

Det tas ingen backup. Hvis du leverer en opptaker med filer på vil de
bli slettet.

### Bærbare pcer

Laptopene våre blir reinstallert jevnlig og ingen filer blir tatt vare
på.

### Stasjonære PCer

Det er tre mapper på pcene våre som er egnet til å lagre filer i, alle
ligger på skrivebordet. Disse mappene er de eneste det tas backup av og
som blir tatt vare på ved oppgraderinger/endringer på pcene. Alle andre
filer kan slettes uten forvarsel.

**Documents** - Lag en mappe med navnet ditt og putt filer der.

**opptak** - PlayIt Recorder lagrer automatisk opptak til denne mappen.
Opptak eldre enn en måned blir slettet. Husk derfor å arkivere ferdige
sendinger til airtime og å flytte andre opptak du trenger til documents.

**opptak_klippet** - Her kan du lagre raske redigeringsjobber. Blir
slettet etter en måned.

## Verktøy

Disse verktøyene er installert på alle pcer.

- [Audacity](https://www.audacityteam.org) - Lydredigering
- [Musicbrainz picard](https://musicbrainz.org) - Merking av lydfiler
- [Firefox](https://www.mozilla.org/en-US/firefox/new) - Nettleser
- [VLC media player](https://www.videolan.org/vlc/index.html) - Mediespiller
- [Libreoffice]https://www.libreoffice.org) - Tekstformatering, regneark ++

### Stasjonære

I tillegg til verktøyene over er dette også installert:

- [Hindenburg journalist](https://hindenburg.com/products/hindenburg-journalist) - Lydredigering
- [Freac](https://www.freac.org/) - CD-ripping og filkonvertering
- [Notepad++](https://notepad-plus-plus.org) - Enkle notater
- [7zip](https://www.7-zip.org) - Komprimering
- [Sumatra](https://www.sumatrapdfreader.org/free-pdf-reader.html) - PDF-leser
- [Infrarecorder](http://infrarecorder.org) - Brenne CDer

### Bærbare

I tillegg til verktøyene over er dette også installert:

- [Soundconverter](https://soundconverter.org) - Konvertering av lydfiler
- [Ardour](https://ardour.org) - Lydredigering
