# Oversikt over dokumentasjon

Her finner du all dokumentasjon som er skrevet for medlemmer av RadiOrakel.
Hvis du er ny kan du starte med å lese [velkomstbrevet](velkommen.md).

Kom gjerne med forslag til endringer eller gjør endringer selv ved å trykke på pennen. Du må lage en gitlab-konto for å gjøre endringer.
