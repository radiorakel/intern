# Introduksjon

## Hva er Airtime?

Airtime er et program installert på en server i studio. Programmet lar
oss logge på en nettside, laste opp lyd og legge sendinger i en
kalender. Airtime sørger så for at det blir lyd på lufta med rett
innhold til rett tid. Airtime er også måten vi arkiverer egenprodusert
innhold.

## Hvorfor?

Airtime kan automatisere teknikervakter slik at vi ikke er avhengig av å
ha folk tilstede i studio for å få lyd på lufta. Det gjør at folk kan
være bakvakt istedenfor og frigjør kapasitet til andre (sårt
tiltrengte!) oppgaver.

## Redaksjonens ansvar

Hver redaksjon har ansvar for egen sendetid. Det betyr enten live
sending, reprise eller ansvar for å legge inn musikk og jingler.
Redaksjonene har også ansvar for at filer som lastes opp til Airtime er
riktig merket.
