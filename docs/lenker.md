# Nyttige lenker

## Promo

RadiOrakels sosiale medier, nettside og patreon-konto. Spre ivei!

- [radiorakel.no/kurs](https://radiorakel.no/kurs)
- [radiorakel.no/bidra](https://radiorakel.no/bidra)
- [patreon.com/radiorakel](https://patreon.com/radiorakel)
- [facebook.com/radiorakel](https://facebook.com/radiorakel/)
- [twitter.com/radiorakel](https://twitter.com/radiorakel)
- [instagram.com/radiorakel](https://www.instagram.com/radiorakel/)
- [radiorakel.no/livestream](https://radiorakel.no/livestream)

## Interne verktøy

- [slack.radiorakel.no](https://slack.radiorakel.no) -
  Internkommunikasjon
- [reservasjon.radiorakel.no](https://reservasjon.radiorakel.no) -
  Reservere rom og utstyr
- [airtime.radiorakel.no](https://airtime.radiorakel.no/) -
  Avviklingsverktøy
- [radiorakel.no/login](https://radiorakel.no/login) - Publisere på
  nettsiden
- [webmail.radiorakel.no](https://webmail.radiorakel.no) - Epost (verv
  og redaksjoner)

## Eksterne guider

- [Manual Hindenburg
  journalist](https://hindenburg.com/support/guides?pid=1128)
