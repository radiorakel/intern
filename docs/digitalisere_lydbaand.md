# Digitalisering av lydbånd

## Forberedelser

Studioet vi bruker til digitalisering er låst, snakk med redaktør for
nøkkel.

Sjekk arkivmappen i tekniker-pcen på studio for å se siste lydbånd som
er blitt digitalisert. De er arkivert 0000 til 0780, og går bakover i
tid.

Gå på loftet og finn lydbåndene og loggboken for året du jobber med
(noen år mangler loggbok). De ligger i pappkasser stablet mot veggen og
har hvite og oransje omslag.

## Digitalisering

Gå ned i studio og skru på

- lydbåndleseren
- redigeringsutstyret
- høytaleren
- datamaskinen

Still inn redigeringsutstyret -- lyd inn på 2, og kablene (23+24M skal
kobles til A80 V+H) \[bilde\]. De hvite knappene ved lyd inn er for å
justere volumet på høytalerne.

Putt et bånd inn i maskinen. Pass på at båndet leses fra riktig side.

Innstill farten på avleseren på til 7 1/2 eller 15 (se på båndomslaget
eller hør etter og juster). OBS! Farten kan skifte midt i båndet (stop,
spol tilbake, endre fart, spill), eller gå fra den andre enden
(baklengs). Da spoler man hele veien frem og skifter båndet over og snur
den opp-ned for å spille fra den andre enden.

<!-- prettier-ignore -->
!!! warning
    ALDRI trykk REC. Da tar du opp over det som er på båndet.

Åpne reaper, velg **File --\> Template --\> Radio digitalisering**. Velg
F-lagringen, og mappen 'radiorakel digitalisering'. Gi filen navn
etter arkivsystemet, altså 0024, 0610 el.l.

Trykk **ARM** og **REC** i Reaper. Trykk PLAY på avspillingsmaskinen.

Nyt arkivmaterialet! Noter info om program og tidspunkter hvor det er
lovende ting til omsending. Noter informasjon i loggboken eller
båndomslaget.

Inne på Reaper, velg **File --\> consolidate/export**.

48000 (CD kvalitet), 24 bit, Fast, FLAC. Åpne F: og Radiorakel
Digitalisering. Overfør Flac-filene til ekstern harddisk eller
minnepinne (usb-inngang i datamaskinen under bordet, bruk øverste av
to).

Når båndet er spilt ferdig, spol tilbake og legg båndet tilbake i
omslaget. Teip en lapp med \'digitalisert\' og informasjon om hvor
materialet er blitt lagt, på omslaget.

## Arkivering

Gå til en av pcene våre i tredje etasje og logg på som brukeren arkiv.
Overfør flac-filene til **This Pc -\> arkiv**.

Merk filene med MusicBrainz Picard. Se [merking](airtime/formater_og_merking.md) for mer info.

Putt viktig notert info som programleder og tekniker for sendingen i
tittelen.

Klipp ut interessante innslag med hindenburg eller audacity, eksporter
til flac og merk de med picard. Kopier filene til arkiv.
