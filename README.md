# Dokumentasjon for medlemmer av RadiOrakel

Dokumentasjonen er skrevet i markdown og kompileres med mkdocs før publisering på [intern.radiorakel.no](https://intern.radiorakel.no).

Temaet som brukes er [mkdocs-material](https://squidfunk.github.io/mkdocs-material/).

For å endre rekkefølgen på filer bruker vi [awesome-pages](https://github.com/lukasgeiter/mkdocs-awesome-pages-plugin).

## Jobbe lokalt

Installer nødvendige pakker med `pip install -r requirements.txt`.
Kjør forhåndsvisning med `mkdocs serve`.
